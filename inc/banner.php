        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Antirrespingo de Solda</h2>
                <p>O antirrespingo de solda MIG/MAG é um produto muito utilizado em indústrias, principalmente para proteger peças metálicas contra a adesão dos respingos de solda, especialmente em superfícies de acabamento.</p>
                <a href="<?=$url?>antirrespingo-de-solda" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Desengraxante Industrial</h2>
                <p>O desengraxante industrial tem como finalidade assegurar um melhor acabamento da superfície e propiciar uma eficaz remoção nos processos de desengraxe. </p>
                <a href="<?=$url?>desengraxante-industrial" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>EMULSÃO DE SILICONE ANTIESPUMANTE!</h2>
                <p>A emulsão de silicone antiespumante é um versátil produto que visa conter a formação de espumas em sistemas. O produto tem a capacidade de suprimir a indesejada espuma formada nos processos industriais e/ou em estações de tratamento de efluentes.</p>
                <a href="<?=$url?>emulsao-de-silicone-antiespumante" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->